#ifndef FILEMANAGEMENT_HPP_
#define FILEMANAGEMENT_HPP_

#include <string>
void saveFile(const std::string file_string, const char* file_name, bool* changeSaveDummy);
std::string openFile(const char* file_name);

#endif

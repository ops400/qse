#ifndef EDITOR_HPP_
#define EDITOR_HPP_

#include "deps/imgui.h"
#include <string>

typedef struct editorArguments{
    std::string editorString; // como eu sou filha da puta burro do caralho, eu vou usar string para fazer o parse. 
    char filePath[999], fileType[999]; // isso ta começando a ficar complicado.
    bool openSaveAsModal, openOpenFileModal, openAboutWindow, openPreferencesWindow, applyStyle;
    ImGuiStyle style;
} editorArguments; // it's only here so I can make things more "elegant"...

void editor(ImGuiIO& io, const char* fileName, bool shouldOpenFile, ImGuiStyle* passthroughStyle);
void getFileExtension(editorArguments* editArgs); // o jeito mais burro de fazer isso

#endif
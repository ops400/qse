#ifndef WINDOW_HPP_
#define WINDOW_HPP_

#include "deps/imgui.h"
#include "editor.hpp"
#include <string>

#define WINDOW_WIDTH 1240
#define WINDOW_HEIGHT 720
#define WINDOW_NAME "Quite a Simple Editor"
#define VERSION "0.0.3 in the works"

void SaveAsModal(editorArguments* editArgs, bool* changeSaveDummy);
void openFileModal(editorArguments* editArgs);
bool aboutWindow(bool* doIOpen);
void preferencesWindow(editorArguments* editArgs);

#endif

#include "deps/imgui.h"
#include "qse.hpp"
#include "fileManagement.hpp"
#include "editor.hpp"
#include <cstdint>
#include <cstdio>

void SaveAsModal(editorArguments* editArgs, bool* changeSaveDummy){
    ImGui::OpenPopup("Save as");
    if(ImGui::BeginPopupModal("Save as", &editArgs->openSaveAsModal, ImGuiWindowFlags_NoResize)){
        ImGui::SetWindowSize(ImVec2(395, 116));
        ImGui::Text("File path:");
        ImGui::InputText("##fps", editArgs->filePath, sizeof(editArgs->filePath));
        if(ImGui::Button("Save")){
            if(strcmp(editArgs->filePath, "") == 0) strcpy(editArgs->filePath, "dummy.txt");
            saveFile(editArgs->editorString, editArgs->filePath, changeSaveDummy);
            ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
            editArgs->openSaveAsModal = false;
            return;
        }
        ImGui::SameLine();
        if(ImGui::Button("Cancel")) {ImGui::CloseCurrentPopup(); ImGui::EndPopup(); editArgs->openSaveAsModal = false; return;}
        ImGui::EndPopup();
    }
}

void openFileModal(editorArguments* editArgs){
    ImGui::OpenPopup("Open file");
    if(ImGui::BeginPopupModal("Open file", &editArgs->openOpenFileModal, ImGuiWindowFlags_NoResize)){
        ImGui::SetWindowSize(ImVec2(395, 116));
        ImGui::Text("File path:");
        ImGui::InputText("##fpo", editArgs->filePath, sizeof(editArgs->filePath));
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Remember to save the current open file to not lose your data!");
        if(ImGui::Button("Open")){
            editArgs->editorString = openFile(editArgs->filePath);
            ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
            editArgs->openOpenFileModal = false;
            return;
        }
        ImGui::SameLine();
        if(ImGui::Button("Cancel")) {ImGui::CloseCurrentPopup(); ImGui::EndPopup(); editArgs->openOpenFileModal = false; return;}
        ImGui::EndPopup();
    }
}

bool aboutWindow(bool* do_i_open){
    if(do_i_open){
        ImGui::SetNextWindowFocus();
        ImGui::SetNextWindowBgAlpha(0.45f);
        ImGui::Begin("About", NULL, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar);
        ImGui::Text("Quite a Simple Editor (QSE)\n\nBy Henrique Freitas Lopes (ops400)\n\nVersion %s", VERSION);
        if(ImGui::Button("Ok")) {ImGui::End(); return false;}
        ImGui::End();
        if(ImGui::IsKeyDown(ImGuiKey_Enter)){
            return false;
        }
        return true;
    }
    return false;
}

void preferencesWindow(editorArguments* editArgs){
    ImGui::Begin("Preferences", &editArgs->openPreferencesWindow, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar);
    ImGui::BeginMenuBar();
    if(ImGui::BeginMenu("File")){
        if(ImGui::MenuItem("Apply")) editArgs->applyStyle = true;
        if(ImGui::MenuItem("Close")) {ImGui::EndMenu(); ImGui::EndMenuBar(); ImGui::End(); editArgs->openPreferencesWindow = false; return;};
        ImGui::EndMenu();
    }
    ImGui::EndMenuBar();
    unsigned int h = ImGui::GetWindowHeight()-100;
    if(ImGui::BeginTabBar("tab_preferences")){
        if(ImGui::BeginTabItem("Color")){
            ImGui::BeginChild("color", ImVec2(0, h), true);
            ImGui::SeparatorText("Text");
            ImGui::ColorEdit4("Text", (float *)&editArgs->style.Colors[ImGuiCol_Text], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Frame background");
            ImGui::ColorEdit4("FrameBg", (float *)&editArgs->style.Colors[ImGuiCol_FrameBg], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("FrameBgHovered", (float *)&editArgs->style.Colors[ImGuiCol_FrameBgHovered], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("FrameBgActive", (float *)&editArgs->style.Colors[ImGuiCol_FrameBgActive], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Title background");
            ImGui::ColorEdit4("TitleBg", (float *)&editArgs->style.Colors[ImGuiCol_TitleBg], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("TitleBgActive", (float *)&editArgs->style.Colors[ImGuiCol_TitleBgActive], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("TitleBgCollapsed", (float *)&editArgs->style.Colors[ImGuiCol_TitleBgCollapsed], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Popup background");
            ImGui::ColorEdit4("PopupBg", (float *)&editArgs->style.Colors[ImGuiCol_PopupBg], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Window background");
            ImGui::ColorEdit4("WindowBg", (float *)&editArgs->style.Colors[ImGuiCol_WindowBg], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Menu bar background");
            ImGui::ColorEdit4("MenuBarBg", (float *)&editArgs->style.Colors[ImGuiCol_MenuBarBg], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Button");
            ImGui::ColorEdit4("Button", (float *)&editArgs->style.Colors[ImGuiCol_Button], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("ButtonActive", (float *)&editArgs->style.Colors[ImGuiCol_ButtonActive], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("ButtonHovered", (float *)&editArgs->style.Colors[ImGuiCol_ButtonHovered], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Header");
            ImGui::ColorEdit4("Header", (float *)&editArgs->style.Colors[ImGuiCol_Header], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("HeaderActive", (float *)&editArgs->style.Colors[ImGuiCol_HeaderActive], ImGuiColorEditFlags_AlphaBar);
            ImGui::ColorEdit4("HeaderHovered", (float *)&editArgs->style.Colors[ImGuiCol_HeaderHovered], ImGuiColorEditFlags_AlphaBar);
            ImGui::SeparatorText("Tabs");
            ImGui::ColorEdit4("Tab", (float *)&editArgs->style.Colors[ImGuiCol_Tab]);
            ImGui::ColorEdit4("TabActive", (float *)&editArgs->style.Colors[ImGuiCol_TabActive]);
            ImGui::ColorEdit4("TabHovered", (float *)&editArgs->style.Colors[ImGuiCol_TabHovered]);
            ImGui::ColorEdit4("TabUnfocusedActive", (float *)&editArgs->style.Colors[ImGuiCol_TabUnfocusedActive]);
            ImGui::ColorEdit4("TabUnfocused", (float *)&editArgs->style.Colors[ImGuiCol_TabUnfocused]);
            // que código desgraçado
            ImGui::EndChild();
            ImGui::EndTabItem();
        }
        if(ImGui::BeginTabItem("Other Scales")){
            ImGui::BeginChild("other_scales", ImVec2(0, h), true);
            ImGui::SeparatorText("Window");
            ImGui::SliderFloat("Window corners rounding", &editArgs->style.WindowRounding, 0, 12, "%.1f");
            ImGui::SeparatorText("Scroll bar");
            ImGui::SliderFloat("Scroll bar rounding", &editArgs->style.ScrollbarRounding, 0, 12, "%.1f");
            ImGui::SliderFloat("Scroll bar size", &editArgs->style.ScrollbarSize, 10, 20, "%.1f");
            ImGui::SeparatorText("Tab");
            ImGui::SliderFloat("Tab rounding", &editArgs->style.TabRounding, 0, 12, "%.1f");
            ImGui::EndChild();
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }
    if(ImGui::Button("Apply")) editArgs->applyStyle = true;
    ImGui::SameLine();
    if(ImGui::Button("Close")) {ImGui::End(); editArgs->openPreferencesWindow = false; return;}
    ImGui::End();
}

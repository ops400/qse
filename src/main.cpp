#include <cstdio>
#include <raylib.h>
#include "deps/imgui.h"
#include "deps/rlImGui.h"
#include "qse.hpp"
#include "editor.hpp"

int main(int argc, char** argv){
    SetConfigFlags(FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE);
    InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_NAME);
    rlImGuiSetup(true);
    
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
    io.MouseDrawCursor = true;

    ImGui::StyleColorsDark();

    ImGuiStyle* style = &ImGui::GetStyle();
    style->TabRounding = 0;
    style->ScrollbarRounding = 0;

    printf("%s", argv[1]);
    if(argv[1] == NULL) editor(io, NULL, false, style);
    else editor(io, argv[1], true, style);

    rlImGuiShutdown();
    CloseWindow();
    return 0;
}

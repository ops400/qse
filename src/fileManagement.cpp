#include <fstream>
#include <string>
#include "fileManagement.hpp"

void saveFile(const std::string file_string, const char *file_name, bool* changeSaveDummy){
    std::ofstream file_to_save; file_to_save.open(file_name);
    file_to_save << file_string;
    file_to_save.close();
    if(*changeSaveDummy == true) *changeSaveDummy = false;
    else changeSaveDummy = changeSaveDummy;
}

std::string openFile(const char* file_name){
    std::ifstream file_to_open; file_to_open.open(file_name);
    std::string str_main, str_to_cpy;
    if(file_to_open.is_open()){
        while(std::getline(file_to_open, str_main)){
            str_to_cpy += str_main + "\n";
        }
        file_to_open.close();
        return str_to_cpy;
    }
    return "Could not open file!!!";
}

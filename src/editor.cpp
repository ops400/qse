#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <ostream>
#include <raylib.h>
#include <string>
#include <iostream>
#include "deps/imgui.h"
#include "deps/imgui_stdlib.h"
#include "deps/rlImGui.h"
#include "editor.hpp"
#include "qse.hpp"
#include "fileManagement.hpp"

void editor(ImGuiIO& io, const char* fileName, bool shouldOpenFile, ImGuiStyle* passthroughStyle){
    bool firstSaveDummy = false;
    bool shallILiveOn = true;
    
    editorArguments editorFuncInnerArgs;
    strcpy(editorFuncInnerArgs.filePath, "");
    editorFuncInnerArgs.openOpenFileModal = false;
    editorFuncInnerArgs.openSaveAsModal = false;
    editorFuncInnerArgs.openAboutWindow = false;
    editorFuncInnerArgs.openPreferencesWindow = false;
    editorFuncInnerArgs.applyStyle = false;
    editorFuncInnerArgs.style = *passthroughStyle;
    
    if(shouldOpenFile) editorFuncInnerArgs.editorString = openFile(fileName);
    else{
        editorFuncInnerArgs.editorString = "You started Quite a Simple Editor without a file thus thy dummy text is created.\n";
        firstSaveDummy = true;
    }
    if(shouldOpenFile) strcpy(editorFuncInnerArgs.filePath, fileName);
    
    while(!WindowShouldClose() && shallILiveOn){
        getFileExtension(&editorFuncInnerArgs);
        
        BeginDrawing();
        rlImGuiBegin();
        
        ClearBackground(DARKGRAY);
        
        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        ImGui::Begin("editor", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoBringToFrontOnFocus);
        
        ImGui::BeginMenuBar();
        if(ImGui::BeginMenu("File")){
            if(ImGui::MenuItem("Save file", "Ctrl+S")) saveFile(editorFuncInnerArgs.editorString, editorFuncInnerArgs.filePath, &firstSaveDummy);
            if(ImGui::MenuItem("Save as", "Ctrl+Shift+S")) editorFuncInnerArgs.openSaveAsModal = true;
            if(ImGui::MenuItem("Open file", "Ctrl+O")) editorFuncInnerArgs.openOpenFileModal = true;
            ImGui::Separator();
            if(ImGui::MenuItem("Quit", "ESC")) shallILiveOn = false;
            ImGui::EndMenu();
        }
        if(ImGui::BeginMenu("Edit")){
            if(ImGui::MenuItem("Copy the entire thing", "Ctrl+Shift+C")) ImGui::SetClipboardText(editorFuncInnerArgs.editorString.c_str());
            if(ImGui::MenuItem("Paste at the End")) editorFuncInnerArgs.editorString += ImGui::GetClipboardText();
            ImGui::Separator();
            if(ImGui::MenuItem("Preferences")) editorFuncInnerArgs.openPreferencesWindow = true;
            ImGui::EndMenu();
        }
        if(ImGui::MenuItem("About")) editorFuncInnerArgs.openAboutWindow = true;
        ImGui::EndMenuBar();
        
        ImGui::InputTextMultiline("##tx1", &editorFuncInnerArgs.editorString, ImVec2(ImGui::GetWindowWidth()-16, ImGui::GetWindowHeight()-58), ImGuiInputTextFlags_AllowTabInput);
        ImGui::Separator();
        ImGui::Text("File: %s", editorFuncInnerArgs.filePath);
        // ImGui::SameLine(ImGui::GetWindowWidth()-ImGui::CalcTextSize(TextFormat("FPS: %d", GetFPS())).x-10);
        // ImGui::Text("FPS: %d", GetFPS());
        ImGui::SameLine(ImGui::GetWindowWidth() - ImGui::CalcTextSize(TextFormat("File type: %s", editorFuncInnerArgs.fileType)).x - 10);
        ImGui::Text("File type: %s", editorFuncInnerArgs.fileType);

        if(ImGui::IsKeyDown(ImGuiKey_LeftCtrl) && ImGui::IsKeyDown(ImGuiKey_S) && ImGui::IsKeyDown(ImGuiKey_LeftShift) ||
        firstSaveDummy && ImGui::IsKeyDown(ImGuiKey_LeftCtrl) && ImGui::IsKeyDown(ImGuiKey_S)) editorFuncInnerArgs.openSaveAsModal = true;
        if(ImGui::IsKeyDown(ImGuiKey_LeftCtrl) && ImGui::IsKeyDown(ImGuiKey_S) && 
        !ImGui::IsKeyDown(ImGuiKey_LeftShift) && !firstSaveDummy) saveFile(editorFuncInnerArgs.editorString, editorFuncInnerArgs.filePath, &firstSaveDummy);
        if(ImGui::IsKeyDown(ImGuiKey_LeftCtrl) && ImGui::IsKeyDown(ImGuiKey_O)) editorFuncInnerArgs.openOpenFileModal = true;
        if(ImGui::IsKeyDown(ImGuiKey_LeftCtrl) && ImGui::IsKeyDown(ImGuiKey_LeftShift) && ImGui::IsKeyDown(ImGuiKey_C)) ImGui::SetClipboardText(editorFuncInnerArgs.editorString.c_str());
        if(editorFuncInnerArgs.openSaveAsModal) SaveAsModal(&editorFuncInnerArgs, &firstSaveDummy);
        if(editorFuncInnerArgs.openOpenFileModal) openFileModal(&editorFuncInnerArgs);
        if(editorFuncInnerArgs.openAboutWindow) editorFuncInnerArgs.openAboutWindow = aboutWindow(&editorFuncInnerArgs.openAboutWindow);
        if(editorFuncInnerArgs.openPreferencesWindow) preferencesWindow(&editorFuncInnerArgs);
        
        ImGui::End();
        
        // ImGui::Begin("Style", NULL, ImGuiWindowFlags_NoSavedSettings);
        // ImGui::ShowStyleEditor(&editorFuncInnerArgs.style);
        // ImGui::End();
        
        if(editorFuncInnerArgs.applyStyle){
            for(uint8_t i = 0; i < ImGuiCol_COUNT; i++) passthroughStyle->Colors[i] = editorFuncInnerArgs.style.Colors[i];
            passthroughStyle->WindowRounding =  editorFuncInnerArgs.style.WindowRounding;
            passthroughStyle->ScrollbarRounding = editorFuncInnerArgs.style.ScrollbarRounding;
            passthroughStyle->ScrollbarSize = editorFuncInnerArgs.style.ScrollbarSize;
            passthroughStyle->TabRounding = editorFuncInnerArgs.style.TabRounding;
            editorFuncInnerArgs.applyStyle = false;
        }
        
        rlImGuiEnd();
        EndDrawing();
    }
}

void getFileExtension(editorArguments* editArgs){
    if(editArgs->filePath[0] != '\0'){
        unsigned int numberOfDots = 0;
        for(unsigned int i = 0; i < strlen(editArgs->filePath); i++){
            if(editArgs->filePath[i] == '.') numberOfDots++;
        }
        if(numberOfDots > 0){
            unsigned int whereDotsAre[numberOfDots];
            unsigned int elementPlacing = 0;
            for(unsigned int i = 0; i < strlen(editArgs->filePath); i++){
                if(editArgs->filePath[i] == '.'){
                    whereDotsAre[elementPlacing] = i;
                    elementPlacing++;
                }
            }
            char extension[strlen(editArgs->filePath) - (whereDotsAre[numberOfDots-1] + 1) + 1];
            for(unsigned int i = whereDotsAre[numberOfDots-1]; i < strlen(editArgs->filePath)-1; i++){
                extension[i - whereDotsAre[numberOfDots-1]] = editArgs->filePath[i + 1];
            }
            extension[sizeof(extension)-1] = '\0';
            if(strcmp(extension, "cpp") == 0){
                strcpy(editArgs->fileType, "C++\0");
            }
            else if(strcmp(extension, "hpp") == 0){
                strcpy(editArgs->fileType, "C++ Header File\0");
            }
            else if(strcmp(extension, "c") == 0){
                strcpy(editArgs->fileType, "C\0");
            }
            else if(strcmp(extension, "h") == 0){
                strcpy(editArgs->fileType, "C Header File\0");
            }
            else if(strcmp(extension, "js") == 0){
                strcpy(editArgs->fileType, "JavaScript\0");
            }
            else if(strcmp(extension, "css") == 0){
                strcpy(editArgs->fileType, "CSS\0");
            }
            else if(strcmp(extension, "html") == 0){
                strcpy(editArgs->fileType, "HTML\0");
            }
            else if(strcmp(extension, "txt") == 0){
                strcpy(editArgs->fileType, "Text\0");
            }
            else if(strcmp(extension, "java") == 0){
                strcpy(editArgs->fileType, "Java\0");
            }
            else if(strcmp(extension, "bf") == 0){
                strcpy(editArgs->fileType, "BrainFuck\0");
            }
            else{
                strcpy(editArgs->fileType, "Unknown\0");
            }
        }
        else{
            strcpy(editArgs->fileType, "Unknown\0");
        }
    }
    else{
        strcpy(editArgs->fileType, "Unknown\0");
    }
}

#include <cstdio>
#include <cstring>
#include <iostream>
#include <ostream>
#include <vector>

int main(int argc, const char** argv){
    printf("%s\n", argv[1]);
    printf("sizeof argv[1]: %lu\n", strlen(argv[1]));
    std::vector<unsigned int> whereTheLastDot;
    for(unsigned i = 0; i < strlen(argv[1]); i++){
        if(argv[1][i] == '.'){
            whereTheLastDot.push_back(i);
            printf("i=%d\n", i);
        }
    }
    printf("%lu\n", whereTheLastDot.size());
    if(whereTheLastDot.size() > 0){
        printf("%d\n", whereTheLastDot.back());
        char type[(strlen(argv[1]) - whereTheLastDot.back() - 1) + 1];
        printf("size of type: %lu\nindex %d of argv[1]:%c\n", sizeof(type), whereTheLastDot.back()+1, argv[1][whereTheLastDot.back()+1]);
        for(unsigned int i = whereTheLastDot.back() + 1; i < strlen(argv[1]); i++){
            printf("i normal: %d\n", i);
            printf("i ed: %d\n", i - whereTheLastDot.back() - 1);
            type[i - (whereTheLastDot.back() + 1)] = argv[1][i];
        }
        type[sizeof(type) - 1] = '\0';
        printf("%s\n", type);
    }
    return 0;
}
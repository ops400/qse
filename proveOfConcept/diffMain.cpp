#include <cstddef>
#include <cstdio>
#include <cstring>

int main(int argc, char** argv){
    if(argc == 2){
        unsigned int numberOfDots = 0;
        for(unsigned int i = 0; i < strlen(argv[1]); i++){
            if(argv[1][i] == '.') numberOfDots++;
        }
        printf("%d\n", numberOfDots);
        if(numberOfDots > 0){
            unsigned int whereDotsAre[numberOfDots];
            unsigned int elementPlacing = 0;
            for(unsigned int i = 0; i < strlen(argv[1]); i++){
                if(argv[1][i] == '.'){
                    whereDotsAre[elementPlacing] = i;
                    elementPlacing++;
                }
            }
            for(unsigned int i = 0; i < numberOfDots; i++){
                printf("in %d is %d\n", i, whereDotsAre[i]);
            }
            printf("%lu\n", strlen(argv[1]) - (whereDotsAre[numberOfDots-1] + 1) + 1);
            char extension[strlen(argv[1]) - (whereDotsAre[numberOfDots-1] + 1) + 1];
            printf("DEBUG:%lu\n", strlen(argv[1]));
            printf("whereDotAre:%d\n", whereDotsAre[numberOfDots - 1]);
            for(unsigned int i = whereDotsAre[numberOfDots-1]; i < strlen(argv[1])-1; i++){
                printf("argv[1][%d]\nextension[%d]\n", i + 1, i - whereDotsAre[numberOfDots-1]);
                extension[i - whereDotsAre[numberOfDots-1]] = argv[1][i + 1];
            }
            extension[sizeof(extension)-1] = '\0';
            printf("%s\n", extension);
            if(strcmp(extension, "cpp") == 0){
                printf("C++\n");
            }
            else if(strcmp(extension, "hpp") == 0){
                printf("C++ Header File\n");
            }
            else if(strcmp(extension, "c") == 0){
                printf("C\n");
            }
            else if(strcmp(extension, "h") == 0){
                printf("C Header File\n");
            }
            else if(strcmp(extension, "js") == 0){
                printf("JavaScript\n");
            }
            else if(strcmp(extension, "css") == 0){
                printf("CSS\n");
            }
            else if(strcmp(extension, "html") == 0){
                printf("HTML\n");
            }
            else if(strcmp(extension, "txt") == 0){
                printf("Text\n");
            }
            else if(strcmp(extension, "java") == 0){
                printf("Java\n");
            }
            else if(strcmp(extension, "bf") == 0){
                printf("BrainFuck\n");
            }
            else{
                printf("Unknow\n");
            }
        }
        else{
            printf("Unknow\n");
        }
    }
    else{
        printf("Unknow\n");
    }
    return 0;
}

